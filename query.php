
<?php
	error_reporting(1);
	ini_set('display_errors', 1);
	http_response_code(500);
	
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Allow-Credentials: false');
	
	$query_type = isset($_POST['query_type']) && !empty($_POST['query_type']) ? $_POST['query_type'] : null;
	$session_id = isset($_POST['session_id']) && !empty($_POST['session_id']) ? $_POST['session_id'] : null;
	
	if ($session_id == null) {
		http_response_code(401);
		exit;
	}
	
	if ($query_type == null) {
		http_response_code(400);
		exit;
	}
	
	$campfire_repository = file_get_contents("./campfire-repository.json");
	$campfire_repository = json_decode($campfire_repository, true);
	
	if (!isset($campfire_repository['database_id'])) {
		http_response_code(501);
		exit;
	}
	
	$sql_config = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/../campfire.ini', true);
	if ($sql_config == false) {
		http_response_code(500);
		exit;
	}
	
	$sql_config_section_name = "campfire-".$campfire_repository['database_id'];
	
	if (!isset($sql_config[$sql_config_section_name])) {
		http_response_code(421);
		exit;
	}
	
	$pdo = null;
	try {
		$pdo = new PDO("pgsql:host={$sql_config[$sql_config_section_name]['host']};dbname={$sql_config[$sql_config_section_name]['database']}", $sql_config[$sql_config_section_name]['user'], $sql_config[$sql_config_section_name]['password']);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
		$pdo->setAttribute(PDO::NULL_EMPTY_STRING, true);
	}
	catch (PDOException $e) {
		//print_r($e);
		if ($e->getCode() == 7) {
			http_response_code(503);
		}
		else {
			http_response_code(500);
		}
		exit;
	}
	
	unset($sql_config);
	
	$stmt = $pdo->prepare("
SELECT usr.user_id, usr.user_name
FROM users AS usr
WHERE usr.session_id = :arg_session_id;
	");
	$stmt->bindValue(":arg_session_id", $session_id);
	try {
		$stmt->execute();
	}
	catch (PDOException $e) {
		//print_r($e);
		// Invalid UUID.
		if ($e->getCode() == "22P02") {
			http_response_code(401);
		}
		else {
			http_response_code(500);
		}
		exit;
	}
	
	if ($stmt->rowCount() == 0) {
		http_response_code(401);
		exit;
	}
	if ($stmt->rowCount() !== 1) {
		http_response_code(500);
		exit;
	}
	
	$entry = $stmt->fetch();
	$userId = $entry['user_id'];
	$userName = $entry['user_name'];
	
	switch ($query_type) {
		case "music": {
			$music_ids = isset($_POST['music_ids']) ? $_POST['music_ids'] : null;
			
			if ($music_ids == null) {
				http_response_code(400);
				exit;
			}
			
			$music_ids = explode(",", $music_ids);
			for ($i = 0; $i < count($music_ids); $i++) {
				$music_ids[$i] = intval($music_ids[$i]);
				if ($music_ids[$i] <= 0) {
					http_response_code(400);
					exit;
				}
			}
			
			$music_ids = (new \Ds\Set($music_ids))->toArray();
			
			if (count($music_ids) <= 0) {
				http_response_code(400);
				exit;
			}
			
			$arg_music_ids = implode(',', array_fill(0, count($music_ids), '?'));
			
			$stmt = $pdo->prepare("
SELECT
	musfil.music_id, musfil.update_version
	, musfil.music_title, musfil.music_duration, musfil.music_file_created_datetime
	, muslyr.lyrics_en, muslyr.lyrics_de, muslyr.lyrics_ru
	, muslic.license_id
	, musgen.genre_pone
	, musgen.genre_lyrics
	, musgen.genre_instrumental
	, musgen.genre_no_vocals
	, musgen.genre_original
	, musgen.genre_remix
	, musgen.genre_cover
	, musgen.genre_karaoke
	, musgen.genre_mashup
	, musgen.genre_parody
	, musgen.genre_demo
	, musgen.genre_remaster
	, musgen.genre_extended
	, musgen.genre_full
	, musgen.genre_cut
	, musgen.genre_radio_edit
	, musgen.genre_live
	, musgen.genre_vip
	, musgen.genre_acoustic
	, musgen.genre_remade
	, musgen.genre_anniversary
	, musgen.genre_dubstep
	, musgen.genre_wip
	, musgen.genre_bootleg
	, musgen.genre_bit_tune
	, musgen.genre_explicit
	, musgen.genre_censored
	, musgen.genre_vhs
	, (
		SELECT string_agg(musart.artist_id::text || ':' || musart.is_lead_contributor, ',' ORDER BY musart.is_lead_contributor DESC, musart.artist_id ASC)
		FROM music_artists AS musart
		WHERE musart.music_id = musfil.music_id
	) AS artist_ids
	, (
		SELECT string_agg(s1lab.label_id::text, ', ' ORDER BY s1lab.label_name ASC)
		FROM labels AS s1lab
		LEFT JOIN albums AS s1alb ON s1alb.label_id = s1lab.label_id
		LEFT JOIN music_albums AS s1musalb ON s1musalb.album_id = s1alb.album_id
		LEFT JOIN music_labels AS s1muslab ON s1muslab.label_id = s1lab.label_id
		LEFT JOIN music_files AS s1musfil ON s1musfil.music_id = s1musalb.music_id OR s1musfil.music_id = s1muslab.music_id
		WHERE s1musfil.music_id = musfil.music_id
	) AS label_ids
	, (
		SELECT string_agg(musalb.album_id::text || ':' || musalb.track_number, ',' ORDER BY musalb.album_id ASC)
		FROM music_albums AS musalb
		WHERE musalb.music_id = musfil.music_id
	) AS album_ids
	, (
		SELECT string_agg(musimg.image_id::text, ',')
		FROM image_music_files AS musimg
		WHERE musimg.music_id = musfil.music_id
	) AS image_ids
	, (
		SELECT string_agg(url_encode(weblnk.web_link), ',')
		FROM music_web_links AS muslnk
		LEFT JOIN web_links AS weblnk ON weblnk.web_link_id = muslnk.web_link_id
		WHERE muslnk.music_id = musfil.music_id
	) AS web_link_ids
FROM music_files AS musfil
LEFT JOIN music_lyrics AS muslyr ON muslyr.music_id = musfil.music_id
LEFT JOIN music_licenses AS muslic ON muslic.music_id = musfil.music_id
LEFT JOIN music_genres AS musgen ON musgen.music_id = musfil.music_id
WHERE musfil.music_id IN (" . $arg_music_ids . ")
ORDER BY musfil.music_id ASC;
			");
			$stmt->execute($music_ids);
			
			$genreFields = ["genre_pone", "genre_lyrics", "genre_instrumental", "genre_no_vocals", "genre_original", "genre_remix", "genre_cover", "genre_karaoke", "genre_mashup", "genre_parody", "genre_demo", "genre_remaster", "genre_extended", "genre_full", "genre_cut", "genre_radio_edit", "genre_live", "genre_vip", "genre_acoustic", "genre_remade", "genre_anniversary", "genre_dubstep", "genre_wip", "genre_bootleg", "genre_bit_tune", "genre_explicit", "genre_censored", "genre_vhs"];
			$genreNames = [];
			for ($i = 0; $i < count($genreFields); $i++) {
				$genreNames[$i] = substr($genreFields[$i], count("genre_"));
			}
			
			$result = [];
			
			while ($entry = $stmt->fetch()) {
				$resultItem = null;
				$resultItem['music_id'] = $entry['music_id'];
				$resultItem['update_version'] = $entry['update_version'];
				$resultItem['title'] = $entry['music_title'];
				$resultItem['duration'] = $entry['music_duration'];
				$resultItem['created_datetime'] = $entry['music_file_created_datetime'];
				$resultItem['lyrics_en'] = $entry['lyrics_en'];
				$resultItem['lyrics_de'] = $entry['lyrics_de'];
				$resultItem['lyrics_ru'] = $entry['lyrics_ru'];
				$resultItem['license_id'] = $entry['license_id'];
				
				$resultItem['genres'] = [];
				
				for ($i = 0; $i < count($genreFields); $i++) {
					if ($entry[$genreFields[$i]] == true) {
						array_push($resultItem['genres'], $genreNames[$i]);
					}
				}
				
				$resultItem['artist_items'] = preg_split( '/[, ]+/', $entry['artist_ids'], -1, PREG_SPLIT_NO_EMPTY);
				for ($i = 0; $i < count($resultItem['artist_items']); $i++) {
					$resultItem['artist_items'][$i] = explode(":", $resultItem['artist_items'][$i]);
					$resultItem['artist_items'][$i][0] = intval($resultItem['artist_items'][$i][0]);
					$resultItem['artist_items'][$i][1] = ($resultItem['artist_items'][$i][1] === "true");
				}
				
				// FIXME delete
				$resultItem['artist_ids'] = $resultItem['artist_items'];
				
				$resultItem['label_ids'] = preg_split( '/[, ]+/', $entry['label_ids'], -1, PREG_SPLIT_NO_EMPTY);
				for ($i = 0; $i < count($resultItem['label_ids']); $i++) {
					$resultItem['label_ids'][$i] = intval($resultItem['label_ids'][$i]);
				}
				
				$resultItem['album_items'] = preg_split( '/[, ]+/', $entry['album_ids'], -1, PREG_SPLIT_NO_EMPTY);
				for ($i = 0; $i < count($resultItem['album_items']); $i++) {
					$resultItem['album_items'][$i] = explode(":", $resultItem['album_items'][$i]);
					$resultItem['album_items'][$i][0] = intval($resultItem['album_items'][$i][0]);
					$resultItem['album_items'][$i][1] = intval($resultItem['album_items'][$i][1]);
				}
				
				// FIXME delete
				$resultItem['album_ids'] = $resultItem['album_items'];
				
				$resultItem['image_ids'] = preg_split( '/[, ]+/', $entry['image_ids'], -1, PREG_SPLIT_NO_EMPTY);
				for ($i = 0; $i < count($resultItem['image_ids']); $i++) {
					$resultItem['image_ids'][$i] = intval($resultItem['image_ids'][$i]);
				}
				
				$resultItem['web_links'] = preg_split( '/[, ]+/', $entry['web_link_ids'], -1, PREG_SPLIT_NO_EMPTY);
				for ($i = 0; $i < count($resultItem['web_links']); $i++) {
					$resultItem['web_links'][$i] = urldecode($resultItem['web_links'][$i]);
				}
				
				array_push($result, $resultItem);
			}
			
			$resultJson = json_encode($result);
			
			http_response_code(200);
			echo $resultJson;
			exit;
		}
		case "artist": {
			$artist_ids = isset($_POST['artist_ids']) ? $_POST['artist_ids'] : null;
			
			if ($artist_ids == null) {
				http_response_code(400);
				exit;
			}
			
			$artist_ids = explode(",", $artist_ids);
			for ($i = 0; $i < count($artist_ids); $i++) {
				$artist_ids[$i] = intval($artist_ids[$i]);
				if ($artist_ids[$i] <= 0) {
					http_response_code(400);
					exit;
				}
			}
			
			$searchArtistIds = new \Ds\Set($artist_ids);
			$searchedArtistIds = new \Ds\Set();
			
			$result = [];
			
			while ($searchArtistIds->count() > 0) {
				$argArtistIds = implode(',', array_fill(0, $searchArtistIds->count(), '?'));
				
				$stmt = $pdo->prepare("
SELECT
	art.artist_id
	, art.artist_name, art.artist_notes
	, art.artist_name_main_id
	, (
		SELECT string_agg(alb.album_id::text, ',')
		FROM albums AS alb
		WHERE alb.artist_id = art.artist_id
	) AS album_ids
	, (
		SELECT string_agg(artimg.image_id::text, ',')
		FROM image_artists AS artimg
		WHERE artimg.artist_id = art.artist_id
	) AS image_ids
FROM artists AS art
WHERE art.artist_id IN (" . $argArtistIds . ")
ORDER BY art.artist_id ASC;
				");
				$stmt->execute($searchArtistIds->toArray());
				
				$searchedArtistIds = $searchedArtistIds->union($searchArtistIds);
				$searchArtistIds->clear();
				
				while ($entry = $stmt->fetch()) {
					$resultItem = null;
					$resultItem['artist_id'] = $entry['artist_id'];
					$resultItem['name'] = $entry['artist_name'];
					$resultItem['notes'] = $entry['artist_notes'];
					$resultItem['artist_name_main_id'] = intval($entry['artist_name_main_id']);
					if ($resultItem['artist_name_main_id'] > 0) {
						if (!$searchedArtistIds->contains($resultItem['artist_name_main_id'])) {
							$searchArtistIds->add($resultItem['artist_name_main_id']);
						}
					}
					else {
						$resultItem['artist_name_main_id'] = null;
					}
					
					$resultItem['album_ids'] = preg_split( '/[, ]+/', $entry['album_ids'], -1, PREG_SPLIT_NO_EMPTY);
					for ($i = 0; $i < count($resultItem['album_ids']); $i++) {
						$resultItem['album_ids'][$i] = intval($resultItem['album_ids'][$i]);
					}
					
					$resultItem['image_ids'] = preg_split( '/[, ]+/', $entry['image_ids'], -1, PREG_SPLIT_NO_EMPTY);
					for ($i = 0; $i < count($resultItem['image_ids']); $i++) {
						$resultItem['image_ids'][$i] = intval($resultItem['image_ids'][$i]);
					}
					
					array_push($result, $resultItem);
				}
			}
			
			$resultJson = json_encode($result);
			
			http_response_code(200);
			echo $resultJson;
			exit;
		}
		case "label": {
			$label_ids = isset($_POST['label_ids']) ? $_POST['label_ids'] : null;
			
			if ($label_ids == null) {
				http_response_code(400);
				exit;
			}
			
			$label_ids = explode(",", $label_ids);
			for ($i = 0; $i < count($label_ids); $i++) {
				$label_ids[$i] = intval($label_ids[$i]);
				if ($label_ids[$i] <= 0) {
					http_response_code(400);
					exit;
				}
			}
			
			$searchLabelIds = new \Ds\Set($label_ids);
			$searchedLabelIds = new \Ds\Set();
			
			$result = [];
			
			while ($searchLabelIds->count() > 0) {
				$argLabelIds = implode(',', array_fill(0, $searchLabelIds->count(), '?'));
				
				$stmt = $pdo->prepare("
SELECT
	lab.label_id
	, lab.label_name, lab.label_notes
	, (
		SELECT string_agg(alb.album_id::text, ',')
		FROM albums AS alb
		WHERE alb.label_id = lab.label_id
	) AS album_ids
	, (
		SELECT string_agg(labimg.image_id::text, ',')
		FROM image_labels AS labimg
		WHERE labimg.label_id = lab.label_id
	) AS image_ids
FROM labels AS lab
WHERE lab.label_id IN (" . $argLabelIds . ")
ORDER BY lab.label_id ASC;
				");
				$stmt->execute($searchLabelIds->toArray());
				
				$searchedLabelIds = $searchedLabelIds->union($searchLabelIds);
				$searchLabelIds->clear();
				
				while ($entry = $stmt->fetch()) {
					$resultItem = null;
					$resultItem['label_id'] = $entry['label_id'];
					$resultItem['name'] = $entry['label_name'];
					$resultItem['notes'] = $entry['label_notes'];
					
					$resultItem['album_ids'] = preg_split( '/[, ]+/', $entry['album_ids'], -1, PREG_SPLIT_NO_EMPTY);
					for ($i = 0; $i < count($resultItem['album_ids']); $i++) {
						$resultItem['album_ids'][$i] = intval($resultItem['album_ids'][$i]);
					}
					
					$resultItem['image_ids'] = preg_split( '/[, ]+/', $entry['image_ids'], -1, PREG_SPLIT_NO_EMPTY);
					for ($i = 0; $i < count($resultItem['image_ids']); $i++) {
						$resultItem['image_ids'][$i] = intval($resultItem['image_ids'][$i]);
					}
					
					array_push($result, $resultItem);
				}
			}
			
			$resultJson = json_encode($result);
			
			http_response_code(200);
			echo $resultJson;
			exit;
		}
		case "album": {
			$album_ids = isset($_POST['album_ids']) ? $_POST['album_ids'] : null;
			
			if ($album_ids == null) {
				http_response_code(400);
				exit;
			}
			
			$album_ids = explode(",", $album_ids);
			for ($i = 0; $i < count($album_ids); $i++) {
				$album_ids[$i] = intval($album_ids[$i]);
				if ($album_ids[$i] <= 0) {
					http_response_code(400);
					exit;
				}
			}
			
			$searchAlbumIds = new \Ds\Set($album_ids);
			$searchedAlbumIds = new \Ds\Set();
			
			$result = [];
			
			while ($searchAlbumIds->count() > 0) {
				$argAlbumIds = implode(',', array_fill(0, $searchAlbumIds->count(), '?'));
				
				$stmt = $pdo->prepare("
SELECT
	alb.album_id
	, alb.album_name, alb.artist_id, alb.label_id, alb.album_notes
	, (
		SELECT string_agg(albimg.image_id::text, ',')
		FROM image_albums AS albimg
		WHERE albimg.album_id = alb.album_id
	) AS image_ids
	, (
		SELECT string_agg(url_encode(weblnk.web_link), ',')
		FROM album_web_links AS alblnk
		LEFT JOIN web_links AS weblnk ON weblnk.web_link_id = alblnk.web_link_id
		WHERE alblnk.album_id = alb.album_id
	) AS web_link_ids
FROM albums AS alb
WHERE alb.album_id IN (" . $argAlbumIds . ")
ORDER BY alb.album_id ASC;
				");
				$stmt->execute($searchAlbumIds->toArray());
				
				$searchedAlbumIds = $searchedAlbumIds->union($searchAlbumIds);
				$searchAlbumIds->clear();
				
				while ($entry = $stmt->fetch()) {
					$resultItem = null;
					$resultItem['album_id'] = $entry['album_id'];
					$resultItem['name'] = $entry['album_name'];
					$resultItem['artist_id'] = $entry['artist_id'];
					$resultItem['label_id'] = $entry['label_id'];
					$resultItem['notes'] = $entry['album_notes'];
					
					$resultItem['image_ids'] = preg_split( '/[, ]+/', $entry['image_ids'], -1, PREG_SPLIT_NO_EMPTY);
					for ($i = 0; $i < count($resultItem['image_ids']); $i++) {
						$resultItem['image_ids'][$i] = intval($resultItem['image_ids'][$i]);
					}
					
					$resultItem['web_links'] = preg_split( '/[, ]+/', $entry['web_link_ids'], -1, PREG_SPLIT_NO_EMPTY);
					for ($i = 0; $i < count($resultItem['web_links']); $i++) {
						$resultItem['web_links'][$i] = urldecode($resultItem['web_links'][$i]);
					}
					
					array_push($result, $resultItem);
				}
			}
			
			$resultJson = json_encode($result);
			
			http_response_code(200);
			echo $resultJson;
			exit;
		}
		case "latest-200": {
			$stmt = $pdo->prepare("
SELECT *
FROM (
	SELECT musfil.music_id, musfil.update_version
	FROM music_files AS musfil
	ORDER BY musfil.music_id DESC
	LIMIT 200
) AS latest
ORDER BY latest.music_id ASC;
			");
			$stmt->execute();
			
			$result = [];
			
			while ($entry = $stmt->fetch()) {
				$resultItem = null;
				$resultItem['music_id'] = $entry['music_id'];
				$resultItem['update_version'] = $entry['update_version'];
				array_push($result, $resultItem);
			}
			
			$resultJson = json_encode($result);
			
			http_response_code(200);
			echo $resultJson;
			exit;
		}
		case "music-query-1": {
			$filtersJson = isset($_POST['filters']) && !empty($_POST['filters']) ? $_POST['filters'] : null;
			
			if ($filtersJson == null) {
				http_response_code(400);
				exit;
			}
			
			$filters = json_decode($filtersJson);
			
			$queryWhere = [];
			$queryArgI = 0;
			$queryArgs = [];
			
			function FilterTypeText($filterCode, $filterOperation, $filterValue, &$queryWhere, &$queryArgI, &$queryArgs) {
				$queryWhereComponent = "LOWER(" . $filterCode . ")";
				
				switch ($filterOperation) {
					case "==": {
						$queryWhereComponent .= " = ";
						break;
					}
					case "!=": {
						$queryWhereComponent .= " <> ";
						break;
					}
					case "starts_with":
					case "ends_with":
					case "contains": {
						$queryWhereComponent .= " LIKE ";
						break;
					}
					default: {
						return false;
					}
				}
				
				$queryArgVariable = ":arg_" . ++$queryArgI;
				
				array_push($queryArgs, [$queryArgVariable, $filterValue]);
				
				switch ($filterOperation) {
					case "==":
					case "!=": {
						$queryWhereComponent .= "LOWER(" . $queryArgVariable . "::text" . ")";
						break;
					}
					case "starts_with": {
						$queryWhereComponent .= "LOWER(CONCAT(" . $queryArgVariable . "::text" . ",'%'))";
						break;
					}
					case "ends_with": {
						$queryWhereComponent .= "LOWER(CONCAT('%'," . $queryArgVariable . "::text" . "))";
						break;
					}
					case "contains": {
						$queryWhereComponent .= "LOWER(CONCAT('%'," . $queryArgVariable . "::text" . ",'%'))";
						break;
					}
				}
				
				array_push($queryWhere, $queryWhereComponent);
				
				return true;
			}
			
			function FilterTypeNumeric($filterCode, $filterOperation, $filterValue, &$queryWhere, &$queryArgI, &$queryArgs) {
				$queryWhereComponent = $filterCode;
				
				if ($filterCode == "musfil.music_file_created_datetime" && $filterValue == 0) {
					
					switch ($filterOperation) {
						case "==": {
							$queryWhereComponent .= " IS NULL";
							break;
						}
						case "!=": {
							$queryWhereComponent .= " IS NOT NULL";
							break;
						}
						default: {
							return false;
						}
					}
					
					array_push($queryWhere, $queryWhereComponent);
					return true;
				}
				
				switch ($filterOperation) {
					case "<": {
						$queryWhereComponent .= " < ";
						break;
					}
					case "<=": {
						$queryWhereComponent .= " <= ";
						break;
					}
					case "==": {
						$queryWhereComponent .= " = ";
						break;
					}
					case "!=": {
						$queryWhereComponent .= " <> ";
						break;
					}
					case ">=": {
						$queryWhereComponent .= " >= ";
						break;
					}
					case ">": {
						$queryWhereComponent .= " > ";
						break;
					}
					default: {
						return false;
					}
				}
				
				$queryArgVariable = ":arg_" . ++$queryArgI;
				
				array_push($queryArgs, [$queryArgVariable, $filterValue]);
				
				$queryWhereComponent .= $queryArgVariable;
				
				if (
					$filterValue == 0
					&& (
						$filterOperation == "=="
						|| $filterOperation == "<="
						|| $filterOperation == ">="
					)
				) {
					if ($filterCode == "usrprf.music_rating") {
						$queryWhereComponent = "(" . $queryWhereComponent . " OR usrprf.music_rating IS NULL)";
					}
					else if ($filterCode == "usrprf.music_play_count") {
						$queryWhereComponent = "(" . $queryWhereComponent . " OR usrprf.music_play_count IS NULL)";
					}
				}
				
				array_push($queryWhere, $queryWhereComponent);
				return true;
			}
			
			for ($i = 0; $i < count($filters); $i++) {
				switch ($filters[$i]->filterType) {
					case "album": {
						FilterTypeText("alb.album_name", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "in_album": {
						$queryWhereComponent = "alb.album_id";
						switch ($filterOperation) {
							case "==": {
								$queryWhereComponent .= " IS NOT NULL";
								break;
							}
							case "!=": {
								$queryWhereComponent .= " IS NULL";
								break;
							}
							default: {
								exit;
								break;
							}
						}
						
						array_push($queryWhere, $queryWhereComponent);
						
						break;
					}
					case "alts": {
						exit;
						break;
					}
					case "artist": {
						FilterTypeText("art.artist_name", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "genre": {
						exit;
						break;
					}
					case "label": {
						FilterTypeText("lab.label_name", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "license": {
						exit;
						break;
					}
					case "lyrics": {
						FilterTypeText("muslyr.lyrics_en", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "music_created_date": {
						FilterTypeNumeric("musfil.music_file_created_datetime", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "music_flac_md5": {
						$queryWhereComponent = "musfil.music_flac_md5";
						switch ($filters[$i]->filterOperation) {
							case "==": {
								$queryWhereComponent .= " = ";
								break;
							}
							case "!=": {
								$queryWhereComponent .= " <> ";
								break;
							}
							default: {
								exit;
								break;
							}
						}
						
						$queryArgVariable = ":arg_" . ++$queryArgI;
						
						array_push($queryArgs, [$queryArgVariable, $filters[$i]->filterValue]);
						
						$queryWhereComponent .= "LOWER(" . $queryArgVariable . "::text" . ")";
						
						array_push($queryWhere, $queryWhereComponent);
						
						break;
					}
					case "music_id": {
						FilterTypeNumeric("musfil.music_id", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "play_count": {
						FilterTypeNumeric("usrprf.music_play_count", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "rating": {
						FilterTypeNumeric("usrprf.music_rating", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					case "title": {
						FilterTypeText("musfil.music_title", $filters[$i]->filterOperation, $filters[$i]->filterValue, $queryWhere, $queryArgI, $queryArgs);
						break;
					}
					default: {
						exit;
						break;
					}
				}
			}
			
			if (count($queryWhere) <= 0) {
				exit;
			}
			
			$sqlQuery = "
SELECT DISTINCT musfil.music_id, musfil.update_version
FROM music_files AS musfil
LEFT JOIN music_lyrics AS muslyr ON muslyr.music_id = musfil.music_id
LEFT JOIN music_licenses AS muslic ON muslic.music_id = musfil.music_id
LEFT JOIN music_genres AS musgen ON musgen.music_id = musfil.music_id
LEFT JOIN music_albums AS musalb ON musalb.music_id = musfil.music_id
LEFT JOIN albums AS alb ON alb.album_id = musalb.album_id
LEFT JOIN music_artists AS musart ON musart.music_id = musfil.music_id
LEFT JOIN artists AS art ON art.artist_id = musart.artist_id
LEFT JOIN music_labels AS muslab ON muslab.music_id = musfil.music_id
LEFT JOIN labels AS lab ON lab.label_id = muslab.label_id OR lab.label_id = alb.label_id
LEFT JOIN user_music_preferences AS usrprf ON usrprf.music_id = musfil.music_id AND usrprf.user_id = :arg_user_id
WHERE
" .
implode(" AND ", $queryWhere)
. "
ORDER BY musfil.music_id ASC;
			";
			$stmt = $pdo->prepare($sqlQuery);
			$stmt->bindValue(":arg_user_id", $userId);
			for ($i = 0; $i < count($queryArgs); $i++) {
				$stmt->bindValue($queryArgs[$i][0], $queryArgs[$i][1]);
			}
			$stmt->execute();
			
			$result = [];
			
			while ($entry = $stmt->fetch()) {
				$resultItem = null;
				$resultItem['music_id'] = $entry['music_id'];
				$resultItem['update_version'] = $entry['update_version'];
				array_push($result, $resultItem);
			}
			
			$resultJson = json_encode($result);
			
			http_response_code(200);
			echo $resultJson;
			exit;
		}
		case "music_rating": {
			$music_id = isset($_POST['music_id']) ? intval($_POST['music_id']) : null;
			$rating = isset($_POST['music_rating']) ? intval($_POST['music_rating']) : null;
			$music_ids = isset($_POST['music_ids']) ? $_POST['music_ids'] : null;
			
			if ($music_ids != null) {
				$music_ids = explode(",", $music_ids);
				for ($i = 0; $i < count($music_ids); $i++) {
					$music_ids[$i] = intval($music_ids[$i]);
					if ($music_ids[$i] <= 0) {
						http_response_code(400);
						exit;
					}
				}
				
				$music_ids = (new \Ds\Set($music_ids))->toArray();
			}
			
			if ($music_id <= 0) {
				$music_id = null;
			}
			
			if ($rating === -1) {
				$rating = null;
			}
			
			if (($music_id === null) === (count($music_ids) <= 0)) {
				http_response_code(400);
				exit;
			}
			
			if (count($music_ids) > 0 && $rating !== null) {
				http_response_code(400);
				exit;
			}
			
			if ($rating < 0 || $rating > 10) {
				http_response_code(400);
				exit;
			}
			
			if ($rating === null) {
				if ($music_id !== null) {
					$stmt = $pdo->prepare("
SELECT usrprf.music_rating
FROM user_music_preferences AS usrprf
WHERE usrprf.user_id = :arg_user_id AND usrprf.music_id = :arg_music_id
					");
					$stmt->bindValue(":arg_user_id", $userId);
					$stmt->bindValue(":arg_music_id", $music_id);
					$stmt->execute();
					
					$savedMusicRating = 0;
					
					if ($stmt->rowCount() === 1) {
						$entry = $stmt->fetch();
						$savedMusicRating = $entry['music_rating'];
					}
					else if ($stmt->rowCount() !== 0) {
						http_response_code(500);
						exit;
					}
					
					$result = null;
					$result['music_id'] = $music_id;
					$result['music_rating'] = $savedMusicRating;
					
					$resultJson = json_encode($result);
					
					http_response_code(200);
					echo $resultJson;
					exit;
				}
				
				$arg_music_ids = implode(',', array_fill(0, count($music_ids), '?'));
				
				$arguments = [$userId];
				array_push($arguments, ...$music_ids);
				
				$stmt = $pdo->prepare("
SELECT usrprf.music_id, usrprf.music_rating
FROM user_music_preferences AS usrprf
WHERE usrprf.user_id = ? AND usrprf.music_id IN (" . $arg_music_ids . ")
ORDER BY usrprf.music_id ASC;
				");
				$stmt->execute($arguments);
				
				$result = [];
				
				while ($entry = $stmt->fetch()) {
					$resultItem = null;
					$resultItem['music_id'] = $entry['music_id'];
					$resultItem['music_rating'] = $entry['music_rating'];
					
					array_push($result, $resultItem);
				}
				
				$resultJson = json_encode($result);
				
				http_response_code(200);
				echo $resultJson;
				exit;
			}
			else if ($rating === 0) {
				$stmt = $pdo->prepare("
DELETE FROM user_music_preferences AS usrprf
WHERE usrprf.user_id = :arg_user_id AND usrprf.music_id = :arg_music_id
				");
				$stmt->bindValue(":arg_user_id", $userId);
				$stmt->bindValue(":arg_music_id", $music_id);
				$stmt->execute();
				
				$result = null;
				$result['music_id'] = $music_id;
				$result['music_rating'] = 0;
				
				$resultJson = json_encode($result);
				
				http_response_code(200);
				echo $resultJson;
				exit;
			}
			else {
				$stmt = $pdo->prepare("
INSERT INTO user_music_preferences(user_id, music_id, music_rating)
VALUES (:arg_user_id, :arg_music_id, :arg_music_rating)
ON CONFLICT (user_id, music_id) DO
UPDATE SET music_rating = :arg_music_rating;
				");
				$stmt->bindValue(":arg_user_id", $userId);
				$stmt->bindValue(":arg_music_id", $music_id);
				$stmt->bindValue(":arg_music_rating", $rating);
				$stmt->execute();
				
				$result = null;
				$result['music_id'] = $music_id;
				$result['music_rating'] = $rating;
				
				$resultJson = json_encode($result);
				
				http_response_code(200);
				echo $resultJson;
				exit;
			}
		}
		default: {
			http_response_code(400);
			exit;
		}
	}
	
?>
