
<?php
	error_reporting(1);
	ini_set('display_errors', 1);
	http_response_code(500);
	
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Allow-Credentials: false');
	
	function GetFilePathFileExtension($file_filepath)
	{
		$fileExt = "";
		$iFilename = strrpos($file_filepath, "/");
		if ($iFilename >= 0) {
			$iFilenameExt = strrpos($file_filepath, ".", $iFilename);
			if ($iFilenameExt > $iFilename) {
				$fileExt = strtolower(substr($file_filepath, $iFilenameExt));
			}
		}
		
		return $fileExt;
	}
	
	function GetPathParentDirectory($path)
	{
		$parent = null;
		$iPath = strrpos($path, "/");
		if ($iPath >= 0) {
			if ($iPath == strlen($path)) {
				$iPath = strrpos($path, "/", $iPath);
			}
			$parent = substr($path, 0, $iPath);
		}
		
		return $parent;
	}
			
	function EnsureDirectoryExists($path)
	{
		if (!is_dir($path)) {
			$pathParent = GetPathParentDirectory($path);
			if (strlen($pathParent) <= 0 || $pathParent == "/" || $pathParent == ".") {
				return true;
			}
			$result = EnsureDirectoryExists($pathParent);
			if (!$result) {
				return false;
			}
			shell_exec("mkdir --mode=0775 " . escapeshellarg($path));
			return is_dir($path);
		}
		else {
			return true;
		}
	}
	
	$object_type = isset($_POST['object_type']) ? $_POST['object_type'] : null;
	$object_id = isset($_POST['object_id']) ? $_POST['object_id'] : null;
	$session_id = isset($_POST['session_id']) ? $_POST['session_id'] : null;
	
	if ($session_id == null) {
		http_response_code(401);
		exit;
	}
	
	if ($object_type == null) {
		http_response_code(400);
		exit;
	}
	
	if ($object_id == null) {
		http_response_code(400);
		exit;
	}
	
	$object_id = intval($object_id);
	if ($object_id <= 0) {
		http_response_code(400);
		exit;
	}
	
	$campfire_repository = file_get_contents("./campfire-repository.json");
	$campfire_repository = json_decode($campfire_repository, true);
	
	if (!isset($campfire_repository['database_id'])) {
		http_response_code(501);
		exit;
	}
	
	$sql_config = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/../campfire.ini', true);
	if ($sql_config == false) {
		http_response_code(500);
		exit;
	}
	
	$sql_config_section_name = "campfire-".$campfire_repository['database_id'];
	
	if (!isset($sql_config[$sql_config_section_name])) {
		http_response_code(421);
		exit;
	}
	
	$pdo = null;
	try {
		$pdo = new PDO("pgsql:host={$sql_config[$sql_config_section_name]['host']};dbname={$sql_config[$sql_config_section_name]['database']}", $sql_config[$sql_config_section_name]['user'], $sql_config[$sql_config_section_name]['password']);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
		$pdo->setAttribute(PDO::NULL_EMPTY_STRING, true);
	}
	catch (PDOException $e) {
		//print_r($e);
		if ($e->getCode() == 7) {
			http_response_code(503);
		}
		else {
			http_response_code(500);
		}
		exit;
	}
	
	unset($sql_config);
	
	$stmt = $pdo->prepare("
SELECT usr.user_id, usr.user_name
FROM users AS usr
WHERE usr.session_id = :arg_session_id;
	");
	$stmt->bindValue(":arg_session_id", $session_id);
	try {
		$stmt->execute();
	}
	catch (PDOException $e) {
		//print_r($e);
		// Invalid UUID.
		if ($e->getCode() == "22P02") {
			http_response_code(401);
		}
		else {
			http_response_code(500);
		}
		exit;
	}
	
	if ($stmt->rowCount() == 0) {
		http_response_code(401);
		exit;
	}
	if ($stmt->rowCount() !== 1) {
		http_response_code(500);
		exit;
	}
	
	$entry = $stmt->fetch();
	$userName = $entry['user_name'];
	
	$serveFile = null;
	
	switch ($object_type) {
		case "image": {
			$serveFilePath = "./campfire-data/images/";
			if (!file_exists($serveFilePath)) {
				break;
			}
			
			$fileMatches = scandir($serveFilePath);
			foreach ($fileMatches as $key => $value) {
				if (strpos($value, $object_id."-") === 0) {
					$serveFile = realpath($serveFilePath . $value);
					break;
				}
			}
			
			break;
		}
		case "music": {
			$serveFilePath = "./campfire-data/music-raw/";
			
			$stmt = $pdo->prepare('SELECT music_filepath FROM music_files WHERE music_id = :arg_music_id;');
			$stmt->bindValue(':arg_music_id', $object_id);
			$stmt->execute();
			
			if ($stmt->rowCount() == 0) {
				http_response_code(404);
				exit;
			}
			if ($stmt->rowCount() !== 1) {
				http_response_code(500);
				exit;
			}
			
			$entry = $stmt->fetch();
			$musicFilepath = $entry[0];
			
			$serveFile = $serveFilePath . $musicFilepath;
			$serveFileExt = GetFilePathFileExtension($serveFile);
			switch ($serveFileExt) {
				case ".flac":
				case ".wav":
				case ".mp3": {
					$serveFileMP3 = "./campfire-data/music-app/" . substr($serveFile, strlen($serveFilePath), 0 - strlen($serveFileExt)) . ".mp3";
					
					$fp = fopen($serveFileMP3, 'rb');
					if (!$fp) {
						$currentLocale = setlocale(LC_ALL, 0);
						// Fixes special characters from being filtered out from escapeshellarg(...).
						setlocale(LC_CTYPE, 'en_AU.utf8');
						
						$serveFileMP3Path = GetPathParentDirectory($serveFileMP3);
						if (EnsureDirectoryExists($serveFileMP3Path)) {
							shell_exec("ffmpeg -i " . escapeshellarg($serveFile) . " -ab 320k -map a -map_metadata -1 -fflags +bitexact -flags:v +bitexact -flags:a +bitexact " . escapeshellarg($serveFileMP3));
							$fp = fopen($serveFileMP3, 'rb');
						}
						
						setlocale(LC_CTYPE, $currentLocale);
					}
					
					if ($fp) {
						fclose($fp);
						$serveFile = $serveFileMP3;
					}
					else {
						http_response_code(400);
						exit;
					}
					
					break;
				}
			}
			
			break;
		}
		default: {
			http_response_code(400);
			exit;
		}
	}
	
	if ($serveFile == null) {
		http_response_code(404);
		exit;
	}
	
	// open the file in a binary mode.
	$fp = fopen($serveFile, 'rb');
	if (!$fp) {
		http_response_code(404);
		//echo $serveFile;
		exit;
	}
	
	$serveFileExt = GetFilePathFileExtension($serveFile);
	
	// send the right headers.
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache"); // keeps ie happy.
	header("Content-Disposition: attachment; filename= " . $object_id . $serveFileExt);
	switch ($serveFileExt) {
		case ".flac": {
			header("Content-Type: audio/flac");
			break;
		}
		case ".mp3": {
			header("Content-Type: audio/mpeg");
			break;
		}
		case ".png": {
			header("Content-Type: image/png");
			break;
		}
		case ".jpg":
		case ".jpeg": {
			header("Content-Type: image/jpeg");
			break;
		}
		case ".gif": {
			header("Content-Type: image/gif");
			break;
		}
		default: {
			header("Content-Type: application/octet-stream");
			break;
		}
	}
	header("Content-Length: " . filesize($serveFile));
	header('Content-Transfer-Encoding: binary');
	
	http_response_code(200);
	
	ob_end_clean(); // required here or large files will not work.
	fpassthru($fp);
	
	exit;
	
?>
