
-- DROP ROLE IF EXISTS campfire_admin;

CREATE ROLE campfire_admin WITH
	LOGIN
	NOSUPERUSER
	INHERIT
	NOCREATEDB
	NOCREATEROLE
	NOREPLICATION;

-- DROP ROLE IF EXISTS campfire_script;

CREATE ROLE campfire_script WITH
	LOGIN
	NOSUPERUSER
	INHERIT
	NOCREATEDB
	NOCREATEROLE
	NOREPLICATION
	CONNECTION LIMIT 6
	ENCRYPTED PASSWORD 'SCRAM-SHA-256$hash-data-goes-here=';

-- DROP DATABASE IF EXISTS campfire;

CREATE DATABASE campfire
	WITH
	OWNER = campfire_admin
	ENCODING = 'UTF8'
	LC_COLLATE = 'en_AU.UTF-8'
	LC_CTYPE = 'en_AU.UTF-8'
	TABLESPACE = pg_default
	CONNECTION LIMIT = -1
	IS_TEMPLATE = False;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "url_encode";

-- Music Files.

CREATE SEQUENCE IF NOT EXISTS seq_music_file_id;

CREATE TABLE IF NOT EXISTS music_files (
	music_id integer DEFAULT nextval('seq_music_file_id') PRIMARY KEY NOT NULL
	, music_filepath text UNIQUE NOT NULL
	--, music_sha256 varchar(64) UNIQUE NOT NULL
	, music_flac_md5 varchar(32) DEFAULT NULL
	, music_file_added_datetime timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL
	, music_title text NOT NULL
	, music_file_created_datetime timestamp DEFAULT NULL
	, music_duration interval NOT NULL
	, update_version integer DEFAULT 1
);

CREATE UNIQUE INDEX IF NOT EXISTS music_file_flac_md5_idx ON music_files (music_flac_md5);

-- Music Archives (Albums). This exists mainly for archival and tracking reasons.

CREATE SEQUENCE IF NOT EXISTS seq_music_archive_id;

CREATE TABLE IF NOT EXISTS music_archives (
	music_archive_id integer DEFAULT nextval('seq_music_archive_id') PRIMARY KEY NOT NULL
	, music_archive_filepath text UNIQUE NOT NULL
	, music_archive_sha256 varchar(64) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS music_archive_files (
	music_archive_id integer NOT NULL
	, CONSTRAINT fk_music_archive_id FOREIGN KEY (music_archive_id) REFERENCES music_archives (music_archive_id) ON UPDATE CASCADE ON DELETE CASCADE
	, music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS music_archive_files_idx ON music_archive_files (music_archive_id, music_id);

-- Artists.

CREATE SEQUENCE IF NOT EXISTS seq_artist_id;

CREATE TABLE IF NOT EXISTS artists (
	artist_id integer DEFAULT nextval('seq_artist_id') PRIMARY KEY NOT NULL
	, artist_name text UNIQUE NOT NULL
	, artist_name_main_id integer DEFAULT NULL
	, FOREIGN KEY (artist_name_main_id) REFERENCES artists (artist_id) ON UPDATE CASCADE ON DELETE SET NULL
	, artist_notes text DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS music_artists (
	music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, artist_id integer NOT NULL
	, CONSTRAINT fk_artist_id FOREIGN KEY (artist_id) REFERENCES artists (artist_id) ON UPDATE CASCADE ON DELETE CASCADE
	, is_lead_contributor boolean DEFAULT TRUE
);

CREATE UNIQUE INDEX IF NOT EXISTS music_artists_idx ON music_artists (music_id, artist_id);

-- Labels (P@D, ASOS, etc.).

CREATE SEQUENCE IF NOT EXISTS seq_label_id;

CREATE TABLE IF NOT EXISTS labels (
	label_id integer DEFAULT nextval('seq_label_id') PRIMARY KEY NOT NULL
	, label_name text UNIQUE NOT NULL
	, label_notes text DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS music_labels (
	music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, label_id integer NOT NULL
	, CONSTRAINT fk_label_id FOREIGN KEY (label_id) REFERENCES labels (label_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS music_labels_idx ON music_labels (music_id, label_id);

-- Albums.

CREATE SEQUENCE IF NOT EXISTS seq_album_id;

CREATE TABLE IF NOT EXISTS albums (
	album_id integer DEFAULT nextval('seq_album_id') PRIMARY KEY NOT NULL
	, album_name text NOT NULL
	, artist_id integer DEFAULT NULL
	, CONSTRAINT fk_artist_id FOREIGN KEY (artist_id) REFERENCES artists (artist_id) ON UPDATE CASCADE ON DELETE CASCADE
	, label_id integer DEFAULT NULL
	, CONSTRAINT fk_label_id FOREIGN KEY (label_id) REFERENCES labels (label_id) ON UPDATE CASCADE ON DELETE CASCADE
	, album_notes text DEFAULT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS album_name_artist_label_idx ON albums (album_name, artist_id, label_id);

CREATE TABLE IF NOT EXISTS music_albums (
	music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, album_id integer NOT NULL
	, CONSTRAINT fk_album_id FOREIGN KEY (album_id) REFERENCES albums (album_id) ON UPDATE CASCADE ON DELETE CASCADE
	, track_number integer DEFAULT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS music_albums_idx ON music_albums (music_id, album_id);

-- Source Links.

CREATE SEQUENCE IF NOT EXISTS seq_web_link_id;

CREATE TABLE IF NOT EXISTS web_links (
	web_link_id integer DEFAULT nextval('seq_web_link_id') PRIMARY KEY NOT NULL
	, web_link text UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS music_web_links (
	music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, web_link_id integer NOT NULL
	, CONSTRAINT fk_web_link_id FOREIGN KEY (web_link_id) REFERENCES web_links (web_link_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS music_web_links_idx ON music_web_links (music_id, web_link_id);

CREATE TABLE IF NOT EXISTS album_web_links (
	album_id integer NOT NULL
	, CONSTRAINT fk_album_id FOREIGN KEY (album_id) REFERENCES albums (album_id) ON UPDATE CASCADE ON DELETE CASCADE
	, web_link_id integer NOT NULL
	, CONSTRAINT fk_web_link_id FOREIGN KEY (web_link_id) REFERENCES web_links (web_link_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS album_web_links_idx ON album_web_links (album_id, web_link_id);

-- Images.

CREATE SEQUENCE IF NOT EXISTS seq_image_id;

CREATE TABLE IF NOT EXISTS images (
	image_id integer DEFAULT nextval('seq_image_id') PRIMARY KEY NOT NULL
	, image_filepath text UNIQUE NOT NULL
	, image_data_sha256 varchar(64) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS image_music_files (
	image_id integer NOT NULL
	, CONSTRAINT fk_image_id FOREIGN KEY (image_id) REFERENCES images (image_id) ON UPDATE CASCADE ON DELETE CASCADE
	, music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS image_music_files_idx ON image_music_files (image_id, music_id);

CREATE TABLE IF NOT EXISTS image_albums (
	image_id integer NOT NULL
	, CONSTRAINT fk_image_id FOREIGN KEY (image_id) REFERENCES images (image_id) ON UPDATE CASCADE ON DELETE CASCADE
	, album_id integer NOT NULL
	, CONSTRAINT fk_album_id FOREIGN KEY (album_id) REFERENCES albums (album_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS image_albums_idx ON image_albums (image_id, album_id);

CREATE TABLE IF NOT EXISTS image_artists (
	image_id integer NOT NULL
	, CONSTRAINT fk_image_id FOREIGN KEY (image_id) REFERENCES images (image_id) ON UPDATE CASCADE ON DELETE CASCADE
	, artist_id integer NOT NULL
	, CONSTRAINT fk_artist_id FOREIGN KEY (artist_id) REFERENCES artists (artist_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS image_artists_idx ON image_artists (image_id, artist_id);

CREATE TABLE IF NOT EXISTS image_labels (
	image_id integer NOT NULL
	, CONSTRAINT fk_image_id FOREIGN KEY (image_id) REFERENCES images (image_id) ON UPDATE CASCADE ON DELETE CASCADE
	, label_id integer NOT NULL
	, CONSTRAINT fk_label_id FOREIGN KEY (label_id) REFERENCES labels (label_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS image_labels_idx ON image_labels (image_id, label_id);

-- Music Usage License.

CREATE SEQUENCE IF NOT EXISTS seq_license_id;

CREATE TABLE IF NOT EXISTS licenses (
	license_id integer DEFAULT nextval('seq_license_id') PRIMARY KEY NOT NULL
	, license_name text UNIQUE NOT NULL
	, license_web_link text UNIQUE DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS music_licenses (
	music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, license_id integer NOT NULL
	, CONSTRAINT fk_license_id FOREIGN KEY (license_id) REFERENCES licenses (license_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS music_licenses_idx ON music_licenses (music_id, license_id);

-- Lyrics.

CREATE TABLE IF NOT EXISTS music_lyrics (
	music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, lyrics_en text DEFAULT NULL
	, lyrics_de text DEFAULT NULL
	, lyrics_ru text DEFAULT NULL
);

-- Genres and Categories.
-- I intend to change all this to category_ as genres are a bit subjective and plentiful and can semi implemented for the user as playlists they make.

CREATE TABLE IF NOT EXISTS music_genres (
	music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, genre_pone boolean DEFAULT FALSE
	, genre_lyrics boolean DEFAULT FALSE
	, genre_instrumental boolean DEFAULT FALSE
	, genre_no_vocals boolean DEFAULT FALSE
	, genre_original boolean DEFAULT FALSE
	, genre_remix boolean DEFAULT FALSE
	, genre_cover boolean DEFAULT FALSE
	, genre_karaoke boolean DEFAULT FALSE
	, genre_mashup boolean DEFAULT FALSE
	, genre_parody boolean DEFAULT FALSE
	, genre_demo boolean DEFAULT FALSE
	, genre_remaster boolean DEFAULT FALSE
	, genre_extended boolean DEFAULT FALSE
	, genre_full boolean DEFAULT FALSE
	, genre_cut boolean DEFAULT FALSE
	, genre_radio_edit boolean DEFAULT FALSE
	, genre_live boolean DEFAULT FALSE
	, genre_vip boolean DEFAULT FALSE
	, genre_acoustic boolean DEFAULT FALSE
	, genre_remade boolean DEFAULT FALSE
	, genre_anniversary boolean DEFAULT FALSE
	, genre_dubstep boolean DEFAULT FALSE
	, genre_wip boolean DEFAULT FALSE
	, genre_bootleg boolean DEFAULT FALSE
	, genre_bit_tune boolean DEFAULT FALSE
	, genre_explicit boolean DEFAULT FALSE
	, genre_censored boolean DEFAULT FALSE
	, genre_vhs boolean DEFAULT FALSE
);

-- Alternative Versions of Music (link together related songs).

CREATE TABLE IF NOT EXISTS music_alts (
	music_id_child integer NOT NULL
	, CONSTRAINT fk_music_id_child FOREIGN KEY (music_id_child) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, music_id_parent integer NOT NULL
	, CONSTRAINT fk_music_id_parent FOREIGN KEY (music_id_parent) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS music_alts_idx ON music_alts (music_id_child, music_id_parent);


--CREATE TABLE IF NOT EXISTS music_remakes (
--	music_id_child integer NOT NULL
--	, CONSTRAINT fk_music_id_child FOREIGN KEY (music_id_child) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
--	, music_id_parent integer NOT NULL
--	, CONSTRAINT fk_music_id_parent FOREIGN KEY (music_id_parent) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
--);

--CREATE TABLE IF NOT EXISTS music_remixes (
--	music_id_child integer NOT NULL
--	, CONSTRAINT fk_music_id_child FOREIGN KEY (music_id_child) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
--	, music_id_parent integer NOT NULL
--	, CONSTRAINT fk_music_id_parent FOREIGN KEY (music_id_parent) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
--);

--CREATE TABLE IF NOT EXISTS music_covers (
--	music_id_child integer NOT NULL
--	, CONSTRAINT fk_music_id_child FOREIGN KEY (music_id_child) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
--	, music_id_parent integer NOT NULL
--	, CONSTRAINT fk_music_id_parent FOREIGN KEY (music_id_parent) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
--);

-- Users.

CREATE SEQUENCE IF NOT EXISTS seq_user_id;

CREATE TABLE IF NOT EXISTS users (
	user_id integer DEFAULT nextval('seq_user_id') PRIMARY KEY NOT NULL
	, user_name text UNIQUE NOT NULL
	, artist_id integer DEFAULT NULL
	, CONSTRAINT fk_artist_id FOREIGN KEY (artist_id) REFERENCES artists (artist_id) ON UPDATE CASCADE ON DELETE SET NULL
	, session_id uuid DEFAULT NULL
	, password varchar(60) DEFAULT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS user_name_idx ON users (user_name);
CREATE UNIQUE INDEX IF NOT EXISTS session_id_idx ON users (session_id);

-- User Ratings, Play Speed, Play Count, Skip Parts.

CREATE TABLE IF NOT EXISTS user_music_preferences (
	user_id integer NOT NULL
	, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (user_id) ON UPDATE CASCADE ON DELETE CASCADE
	, music_id integer NOT NULL
	, CONSTRAINT fk_music_id FOREIGN KEY (music_id) REFERENCES music_files (music_id) ON UPDATE CASCADE ON DELETE CASCADE
	, music_rating integer DEFAULT NULL
	, CHECK (music_rating BETWEEN 1 AND 10)
	, music_speed numeric DEFAULT NULL
	, CHECK (music_speed > 0)
	, music_play_count integer DEFAULT 0 NOT NULL
	, CHECK (music_play_count >= 0)
	--, music_skip_parts text DEFAULT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS user_music_preferences_idx ON user_music_preferences (user_id, music_id);

-- Grant Permission for all the new schema.

GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO campfire_script;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO campfire_script;
